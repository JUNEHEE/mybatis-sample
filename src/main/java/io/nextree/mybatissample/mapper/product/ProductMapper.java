package io.nextree.mybatissample.mapper.product;

import io.nextree.mybatissample.model.product.Product;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ProductMapper {
    //
    Product selectProductById(Long id);
    List<Product> selectAllProducts();
    void insertProduct(Product product);
}
