package io.nextree.mybatissample;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@MapperScan(basePackageClasses = MybatisSampleApplication.class)
@EnableSwagger2
@SpringBootApplication
public class MybatisSampleApplication {
    //
    public static void main(String[] args) {
        //
        SpringApplication.run(MybatisSampleApplication.class, args);
    }
}
