package io.nextree.mybatissample.model.product;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Product {
    //
    private Long id;
    private String name;
    private int price;

    public Product(String name, int price) {
        //
        this.name = name;
        this.price = price;
    }
}

