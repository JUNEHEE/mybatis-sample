package io.nextree.mybatissample.model.board;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Board {
    //
    private Long id;
    private String subject;
    private String content;
    private String writerName;
    private long time;

    public Board(String subject, String content, String writerName, long time) {
        //
        this.subject = subject;
        this.content = content;
        this.writerName = writerName;
        this.time = time;
    }
}
