package io.nextree.mybatissample.rest.product;

import io.nextree.mybatissample.model.product.Product;
import io.nextree.mybatissample.service.product.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
public class ProductController {
    //
    @Value("${uri-prefix}")
    private String uriPrefix;
    private final ProductService productService;

    @GetMapping("productList")
    @ResponseBody
    public List<Product> findProductList() {
        //
        return productService.getAllProducts();
    }

    @PostMapping("product")
    @ResponseBody
    public String registerProduct(@RequestBody Product product) {
        //
        productService.addProduct(product);
        return "aa";
    }
}
