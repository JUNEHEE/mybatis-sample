package io.nextree.mybatissample.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Controller
public class IndexController {
    //

    @Value("${uri-prefix}")
    private String uriPrefix;

    @RequestMapping("")
    public String index(Model model,
                        HttpServletRequest request,
                        HttpServletResponse response) {
        //
        model.addAttribute("uriPrefix", uriPrefix);

        return "index";
    }

    @RequestMapping("test")
    public String test(Model model,
                        HttpServletRequest request,
                        HttpServletResponse response) {
        //
        model.addAttribute("uriPrefix", uriPrefix);

        return "test";
    }
}
